#pragma once
#include "./numerical.h"

/*
  Generate Halton sequences
  See http://en.wikipedia.org/wiki/Halton_sequence
*/
double unipolarHaltonAxis(u_int i, u_int radix);
vector<R> unipolarHaltonRow(u_int i, size_t nCols);
double bipolarHaltonAxis(u_int i, u_int radix);
vector<R> bipolarHaltonRow(u_int i, size_t nCols);
vector<R> gaussianHaltonRow(u_int i, size_t nCols);

// The R2 minimum discrepancy sequence.
// http://extremelearning.com.au/unreasonable-effectiveness-of-quasirandom-sequences/

vector<R> r2Factors(u_int d);
vector<R> unipolarR2Row(u_int i, u_int d);
vector<R> bipolarR2Row(u_int i, u_int d);
vector<R> gaussianR2Row(u_int i, size_t d);
