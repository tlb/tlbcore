
# Reasonable user targets
default : build
test ::
build :: stage1
stage1 :: setup
clean ::

include mk/makesystem.inc

# Manual machine setup
# See https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager
# See https://github.com/nodesource/distributions
.PHONY: install.deps install.npm

ifeq ($(UNAME_SYSTEM),Linux)

install.deps ::
	sudo apt-get install curl
	curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
	sudo apt-get update
	sudo apt-get -y install git make python g++ make software-properties-common curl
	sudo apt-get -y install clang-6.0 lldb-6.0 liblapack-dev pkg-config cmake
	sudo update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-6.0 100
	sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-6.0 100
	sudo apt-get -y install nodejs
	sudo apt-get -y install libopenblas-dev liblapack-dev libarpack2-dev libeigen3-dev libapr1-dev libaprutil1-dev libturbojpeg0-dev libuv1-dev libz-dev

endif

ifeq ($(UNAME_SYSTEM),Darwin)
install.deps ::
	brew install rename zopfli ffmpeg trash node tree ack hub git eigen
endif

serve:
	node web/server.js doc

.PHONY: force
force :

push.%: force
	@echo "push $(PWD) to $*:$(subst $(HOME),.,$(PWD))"
	@rsync -a --inplace --relative $(PUSHDIST_EXCLUDES) --delete . $*:$(subst $(HOME),.,$(PWD))/.
