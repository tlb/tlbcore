
'use strict';
const _ = require('lodash');
const assert = require('assert');
const util = require('util');
const {Peggy} = require('./peggy');

describe('Peggy', function() {
  it('should work', function() {

    let p = new Peggy('Test');
    p.rule(`
      program     <- __ (name / SEMI)* %makeProgram
      name        <- [a-zA-Z_] [a-zA-Z0-9_]* __ %makeName
      SEMI     <- ";"         __ %makeOp
    `);

    p.compile();
    console.log(_.keys(p.parser));
  });
});
