
'use strict';
const _ = require('lodash');
const assert = require('assert');
const util = require('util');
const path = require('path');
const canopy = require('canopy'); // https://github.com/jcoglan/canopy

function Peggy(grammarName) {
  this.rules = [];
  this.rule(`
    grammar ${grammarName}
  `);
}
exports.Peggy = Peggy;

Peggy.prototype.rule = function(text) {
  this.rules.push(text.trim());
};

Peggy.prototype.compile = function() {
  let grammar = this.rules.join('\n');

  let builder = canopy.Builders.JavaScript.create('___internal.peg', path.sep);
  let parser  = canopy.compile(grammar, builder);
  let code = parser['___internal.js'];
  let f = new Function('exports', 'require', 'module', code);
  this.parser = {};
  f(this.parser, require, {});
};

Peggy.prototype.parse = function(text, actions) {
  if (!this.parser) {
    this.compile();
  }
  this.parser.parse(text, actions);
};

Peggy.prototype.getLastError = function() {
  return this.parser.Parser.lastError;
};
