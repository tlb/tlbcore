

ifneq ($(BUILD_FOR_WASM),)
UNAME_SYSTEM := wasm-$(BUILD_FOR_WASM)
UNAME_MACHINE := wasm-$(BUILD_FOR_WASM)
else
UNAME_SYSTEM := $(shell uname -s)
UNAME_MACHINE := $(shell uname -m)
# For consistency with node's os.arch()
ifeq ($(UNAME_MACHINE),x86_64)
UNAME_MACHINE := x64
endif
endif

UNAME_SM = $(UNAME_SYSTEM)-$(UNAME_MACHINE)

UNAME_HOST := $(shell uname -n | sed -e 's/\..*//')

PUSHDIST_EXCLUDES += \
	--exclude .git \
	--exclude .gitignore \
	--exclude .gitmodules \
	--exclude .dockerimage \
	--exclude '*.o' \
	--exclude '*.o.d' \
	--exclude '*.pyc' \
	--exclude '*.dylib' \
	--exclude '*.so' \
	--exclude node_modules \
	--exclude '*.jsonlog' \
	--exclude '._*' \
	--exclude .build \
	--exclude .DS_Store \
	--exclude .deps \
	--exclude '.*.pods' \
  --exclude __pycache__ \
	--exclude build.$(UNAME_MACHINE) \
	--exclude .tags \
	--exclude package-lock.json \
	--exclude '.vscode'


ifeq ($(UNAME_SYSTEM),Linux)
PARALLEL := $(shell nproc)
else
ifeq ($(UNAME_SYSTEM),Darwin)
PARALLEL := 8
#$(shell sysctl -n hw.ncpu)
else
PARALLEL := 4
endif
endif

printvar.% : ## Print named variable, eg "make printvar.UNAME_SYSTEM"
	@echo "$*=$($*) (flavor=$(flavor $*) origin=$(origin $*))"

printenv :
	env

printvars : $(foreach x,$(.VARIABLES),printvar.$(x))

.PHONY: help
help: force ## Show make targets
	@perl -n -e 'if (/^([\.a-zA-Z0-9_%-]+)\s*:+.*?##( .*)$$/g) { print sprintf("\033[36m%-30s\033[0m%s\n", $$1, $$2); }' $(MAKEFILE_LIST)
