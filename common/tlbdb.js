'use strict';
const _ = require('lodash');
const async = require('async');
const fs = require('fs');

class TlbDb {
  constructor(fn) {
    this.fn = fn;
    this.data = {};
    this.reviver = null;
    this.writable = null;
  }

  close() {
    if (this.writable) {
      this.writable.close();
      this.writable = null;
    }
    this.data = null;
  }

  load(cb) {
    this.writable = fs.createWriteStream(this.fn, {
      flags: 'a',
    });

    fs.readFile(this.fn, (err, buf) => {
      if (err) return cb(err);

      let pos = 0;
      while (pos < buf.length) {

        let eol = buf.indexOf(10, pos);
        if (eol === -1) eol = buf.length;

        let line = buf.toString('utf8', pos, eol);
        let linej = JSON.parse(line, this.reviver);
        let funcName = `run_${linej.method}`;
        this[funcName].apply(this, linej.params);

        pos = eol+1;
      }


      return cb(null);
    });
  }

  run(method, ...params) {

    let funcName = `run_${method}`;
    this[funcName].apply(this, params);

    let linej = {method, params};
    let line = JSON.stringify(linej) + '\n';

    this.writable.write(line);
  }


  run_set(name, value) {
    _.set(this.data, name, value);
  }

  run_push(name, value) {
    let v = _.get(this.data, name);
    if (!v) {
      v = [];
      _.set(this.data, name, v);
    }
    v.push(value);
  }
}

exports.TlbDb = TlbDb;
