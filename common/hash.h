#pragma once
#include "./std_headers.h"

int32_t jump_consistent_hash(uint64_t key, int32_t num_buckets);

uint64_t MurmurHash64A(const void *key, int len, uint64_t seed);


size_t hashmix(size_t x, size_t v);
