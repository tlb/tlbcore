#include "./chunky.h"
#if defined(__EMSCRIPTEN__)
#include <malloc.h>
#endif
#include <cxxabi.h>

AllocTracker trackChunks("chunks");
AllocTracker trackChunkBytes("chunkBytes");

constexpr int verbose = 0;

ChunkyAlloc::ChunkyAlloc()
{
  trackChunks.a();
  if (verbose) std::cerr << (void *)this << ": constructed\n";
}

ChunkyAlloc::~ChunkyAlloc()
{
  trackChunks.f();
  trackChunkBytes.f(totalChunkAlloc);
  for (auto *ptr : chunks) {
    if (verbose) std::cerr << (void *)this << ": free chunk " << (void *)ptr << "\n";
    free(ptr);
  }
  chunks.clear();
  if (verbose) std::cerr << (void *)this << ": destructed\n";
}

ChunkyAlloc::ChunkyAlloc(ChunkyAlloc &&other) noexcept
  : curChunkPtr(std::move(other.curChunkPtr)),
    curChunkAvail(std::move(other.curChunkAvail)),
    chunks(std::move(other.chunks)),
    objects(std::move(other.objects))
{
  trackChunks.a();
  other.curChunkPtr = nullptr;
  other.curChunkAvail = 0;
  other.chunks.clear();
  other.objects.clear();
  if (verbose) std::cerr << (void *)this << ": moved from " << (void *)&other << "\n";
}

ChunkyAlloc & ChunkyAlloc::operator= (ChunkyAlloc &&other)
{
  if (verbose) std::cerr << (void *)this << ": assigned from " << (void *)&other << "\n";
  curChunkPtr = std::move(other.curChunkPtr); other.curChunkPtr = nullptr;
  curChunkAvail = std::move(other.curChunkAvail); other.curChunkAvail = 0;
  for (auto &it : other.chunks) {
    chunks.push_back(it);
  }
  other.chunks.clear();
  for (auto &it : other.objects) {
    objects.push_back(it);
  }
  other.objects.clear();
  return *this;
}

void *ChunkyAlloc::allocFull(size_t align, size_t s)
{
  // Do large allocations directly from system
  if (s >= 1000000) {
    if (s%64 != 0) {
      s += 64 - (s%64);
    }
    auto *chunk = (U8 *)aligned_alloc(64, s);
    assert(chunk != nullptr);
    trackChunkBytes.a(s);
    chunks.push_back(chunk);
    if (verbose) std::cerr << (void *)this << ": alloc single-use chunk " << (void *)chunk << " size=" << std::hex << s << std::dec << "\n";
    totalChunkAlloc += s;
    return (void *)chunk;
  }
  else if (s > curChunkAvail) {
    size_t chunkSize = 0;
    auto nChunks = chunks.size();
    if (nChunks <= 0 && s <= 512) {
      chunkSize = 4096;
    }
    else if (nChunks <= 1 && s <= 2048) {
      chunkSize = 16384;
    }
    else if (nChunks <= 2 && s <= 8192) {
      chunkSize = 65536;
    }
    else if (s < 32768) {
      chunkSize = 262144;
    }
    else {
      chunkSize = 262144;
      while (s*4 + align*4 + 16 > chunkSize) chunkSize *= 2;
    }
    auto *chunk = (U8 *)aligned_alloc(64, chunkSize);
    if (verbose) std::cerr << (void *)this << ": alloc arena chunk " << (void *)chunk << " size=" << std::hex << chunkSize << std::dec << "\n";
    trackChunkBytes.a(chunkSize);
    chunks.push_back(chunk);
    totalChunkAlloc += chunkSize;
    curChunkPtr = chunk;
    curChunkAvail = chunkSize;
    
    auto misalign = size_t(curChunkPtr) & (align - 1);
    if (misalign) {
      curChunkPtr += align - misalign;
      curChunkAvail -= align - misalign;
    }
  }

  if (s > curChunkAvail) throw logic_error("chunky overrun");

  auto ret = curChunkPtr;
  curChunkPtr += s;
  curChunkAvail -= s;
  return (void *)ret;
}


size_t ChunkyAlloc::totalAlloc() const
{
  return totalChunkAlloc - curChunkAvail;
}

static vector<AllocTracker *> *allTrackers;

AllocTracker::AllocTracker(char const *_name)
  : name(_name)
{
  if (!allTrackers) allTrackers = new vector<AllocTracker *>();
  allTrackers->push_back(this);
}

AllocTracker::~AllocTracker()
{
  if (allTrackers) {
    for (auto &it : *allTrackers) {
      if (it == this) it = nullptr;
    }
  }
}

void AllocTracker::clearCounters()
{
  // but don't clear nAlloc
  nRegularConstructed = 0;
  nCopyConstructed = 0;
  nMoveConstructed = 0;
  nCopyAssigned = 0;
  nMoveAssigned = 0;
}


string AllocTracker::fmtStats()
{
  ostringstream s;
  printStats(s);
  return s.str();
}

void AllocTracker::printStats(ostream &s)
{
#if defined(__EMSCRIPTEN__)
  s << "Alloc: total " << sbrk(0) << "\n";
  auto mi = mallinfo();
  s << "  arena=" << mi.arena << " ordblks=" << mi.ordblks << 
    " hblks=" << mi.hblks << " hblkhd=" << mi.hblkhd <<
    " uordblks=" << mi.uordblks << " fordblks=" << mi.fordblks << "\n";
#else
  s << "Alloc:\n";
#endif

  if (allTrackers) {
    for (auto it : *allTrackers) {
      if (it) {
        int status = -1;
        auto realname = abi::__cxa_demangle(it->name, nullptr, nullptr, &status);
        s << "  " << (realname ? realname : it->name) << 
          " : " << it->nAlloc << 
          " / " << it->nRegularConstructed;
        if (it->nCopyConstructed || it->nMoveConstructed ||
            it->nCopyAssigned || it->nMoveAssigned) {
          s << " regular-constructed + " <<
            it->nCopyConstructed << " copy-constructed + " <<
            it->nMoveConstructed << " move-constructed + " <<
            it->nCopyAssigned << " copy-assigned + " <<
            it->nMoveAssigned << " move-assigned";
        }
        s << "\n";
        free(realname);
      }
    }
  }
}

extern "C" {
  void AllocTracker_printStats() // callable from a debugger
  {
    AllocTracker::printStats(cerr);
  }
}

void AllocTracker::barf(int64_t newTotal)
{
  cerr << "AllocTracker: negative alloc at " << newTotal << " / " << 
    nRegularConstructed << " reg + " <<
    nCopyConstructed << " copy + " <<
    nMoveConstructed << " move\n";
}
