'use strict';
const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const parent_pipe = require('./parent_pipe');

function main() {
  let pp = new parent_pipe.ParentJsonPipe({}, {
    rpc_test1: (v, cb) => {
      cb(null, v+1);
    },
    rpc_test2: (a, b, c, cb) => {
      assert.strictEqual(a, 'abc');
      assert.strictEqual(b, 'def');
      assert.strictEqual(c.ghi, 'jkl');
      cb(null, [[a, b, c], 'foo']);
    },
    rpc_testerr: (cb) => {
      cb(new Error('testerr always raises this error'));
    },
  });
  assert.ok(pp);
}

main();
