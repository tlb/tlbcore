#pragma once

template<typename T>
struct Dyn {
  T *where = nullptr;
  T oldval {};

  Dyn(T *_where, T const &newval)
    : where(_where),
      oldval(*where)
  {
    *where = newval;
  }

  Dyn() = default;

  ~Dyn()
  {
    if (where) {
      *where = oldval;
      where = nullptr;
    }
  }
  void permanent()
  {
    where = nullptr;
  }

  Dyn(Dyn const &) = delete;
  Dyn(Dyn &&other)
    : where(other.where),
      oldval(std::move(other.oldval)) 
  {
    other.where = nullptr;
  }

  Dyn &operator =(Dyn const &) = default;
  Dyn &operator =(Dyn &&) = delete;
};
