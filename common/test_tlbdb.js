'use strict';

const _ = require('lodash');
const assert = require('assert');
const path = require('path');
const fs = require('fs');
const async = require('async');
const tlbdb = require('./tlbdb');

if (0) describe('tlbdb', function() {

  before((cb) => {
    fs.unlink('/tmp/tlbdb-test.jsonl', (_err) => {
      cb(null);
    });
  });

  it('should work', function(cb) {
    let db = new tlbdb.TlbDb('/tmp/tlbdb-test.jsonl');
    db.load((err) => {
      if (err) return cb(err);
      db.run('set', 'foo', 'bar');
      assert.strictEqual(db.data.foo, 'bar');
      db.run('push', 'buz.bag', 'wug');
      assert.strictEqual(db.data.buz.bag[db.data.buz.bag.length-1], 'wug');
      db.close();
      cb(null);

    });
  });

  it('should work', function(cb) {
    let db = new tlbdb.TlbDb('/tmp/tlbdb-test.jsonl');
    db.load((err) => {
      if (err) return cb(err);
      assert.strictEqual(db.data.foo, 'bar');
      assert.strictEqual(db.data.buz.bag[db.data.buz.bag.length-1], 'wug');
      db.close();
      cb(null);
    });
  });

});
