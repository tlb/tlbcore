//-*-C++-*-
#pragma once


#define TLBCORE_VERSION_MAJOR 2
#define TLBCORE_VERSION_MINOR 9
#define TLBCORE_VERSION_PATCH 0


#if defined(__AVR32__)

#if !defined(__EMBEDDED__)
#define __EMBEDDED__
#endif

#include <compiler.h>
#include <machine/endian.h>
#include <stdint.h>
#include <string.h>

#elif defined(__XC32__)

#if !defined(__EMBEDDED__)
#define __EMBEDDED__
#endif

#include <libpic32c.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#ifndef BYTE_ORDER
#include <musl/endian.h>
#endif

#elif defined(__AVR__)

#if !defined(__EMBEDDED__)
#define __EMBEDDED__
#endif

#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
//#include <math.h>
#include <util/delay.h>

#include "atmelstuff/iobits.h"

#elif defined(__EMBEDDED__)

#include <string.h>

#else // not embedded

#if defined(__cplusplus)
#include <cassert>
//#include <cmath>
#include <cstdio>
#include <cstdlib>
#endif

#include <unistd.h>

#if defined(__EMSCRIPTEN__)
#include <endian.h>
#include <stdint.h>
#include <sys/types.h>
#elif defined(__linux__) 
#include <endian.h>
#include <stdint.h>
#else
#include <machine/endian.h>
#endif

#define ffs BOGUS_FFS
#include <string.h>
#undef ffs

#include <time.h>
#if !defined(WIN32)
#include <sys/time.h>
#endif

#include <ctype.h>
#if defined(__FreeBSD__)
#include <ieeefp.h>
#endif
#include <errno.h>
#include <fcntl.h>
#include <float.h>
#include <sys/param.h>
#if !defined(__cplusplus)
#include <stdarg.h>
#endif
#endif // not embedded

#if defined(__cplusplus)
#include <algorithm>
#include <array>
#include <complex>
#include <deque>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <list>
#include <map>
#include <memory>
#include <numeric>
#include <set>
#include <stdexcept>
#include <string>
#include <typeinfo>
#include <unordered_map>
#include <vector>
#include <optional>
#include <variant>
#include <chrono>

using
  std::cout, std::cerr,
  std::vector, std::map, std::deque, std::array, std::set, std::unordered_map, std::list,
  std::tuple, std::make_tuple,
  std::pair, std::make_pair,
  std::initializer_list,
  std::string, std::string_view,
  std::shared_ptr, std::enable_shared_from_this, std::make_shared, 
  std::unique_ptr, std::make_unique,
  std::weak_ptr,
  std::nullptr_t,
  std::ostream, std::ostringstream, std::istream, std::istringstream, std::ofstream, std::ifstream,
  std::overflow_error, std::runtime_error, std::logic_error, std::invalid_argument,
  std::optional, std::nullopt, 
  std::variant, std::monostate,
  std::function,
  std::swap,
  std::complex,
  std::to_string,
  std::sort,
  std::min, std::max, std::abs, std::pow, std::floor, std::round, std::ceil, std::sqrt, std::cbrt,
  std::cos, std::sin, std::tan, std::atan2, std::copysign,
  std::numeric_limits;

using namespace std::string_literals;
namespace chrono = std::chrono;

#endif

#if !defined(__EMBEDDED__)
typedef size_t S;
typedef double R;
typedef float F;
#endif

#if !defined(_COMPILER_H_)
// For compatability with AVR32-land

#if defined(__AVR__) // 8 bit
typedef long int S32;
typedef unsigned long int U32;
#elif defined(__x86_64) // 64 bit
typedef int S32;
typedef unsigned int U32;
typedef long long int S64;
typedef unsigned long long int U64;
#else                   // 32 bit
typedef int S32;
typedef unsigned int U32;
typedef long long int S64;
typedef unsigned long long int U64;
#endif

typedef short int S16;
typedef unsigned short int U16;
typedef signed char S8;
typedef unsigned char U8;
typedef unsigned char Bool;

#if !defined(__cplusplus)
static __inline int min(int a, int b)
{
  if (a < b)
    return a;
  else
    return b;
}
static __inline int max(int a, int b)
{
  if (a > b)
    return a;
  else
    return b;
}
#endif

#endif // compiler_h

#if defined(__EMBEDDED__)

#if !defined(M_PI)
#define M_PI 3.1415926535897931
#define M_2PI 6.2831853071795862
#define M_PI_2 1.5707963267948966
#define M_PI_4 0.78539816339744828
#define M_2_PI 0.63661977236758138
#define M_4_PI 1.2732395447351628
#define M_E 2.7182818284590451
#endif

#endif

#if defined(__cplusplus)
typedef complex<double> complexd;
typedef complex<float> complexf;
#endif

#if !defined(__EMBEDDED__)
#include "./hacks.h"
#endif
