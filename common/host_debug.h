#pragma once
#include "./std_headers.h"

#ifdef __cplusplus

struct WarningFilter {
  unordered_map<string, size_t> warningCount;
  bool operator()(string const &name);
};

#endif
