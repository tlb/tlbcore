/*
 */
#include "./hash.h"

/*
   From http://arxiv.org/pdf/1406.2294v1.pdf
*/
int32_t jump_consistent_hash(uint64_t key, int32_t num_buckets)
{
  int64_t b = -1, j = 0;
  while (j < num_buckets) {
    b = j;
    key = key * 2862933555777941757ULL + 1;
    j = (b + 1) * (double(1LL << 31) / double((key >> 33) + 1));
  }
  return b;
}

// MurmurHash2, 64-bit versions, by Austin Appleby

// MurmurHash2 was written by Austin Appleby, and is placed in the public
// domain. The author hereby disclaims copyright to this source code.

// The same caveats as 32-bit MurmurHash2 apply here - beware of alignment
// and endian-ness issues if used across multiple platforms.

// 64-bit hash for 64-bit platforms

uint64_t MurmurHash64A(const void *key, int len, uint64_t seed)
{
  const uint64_t m = 0xc6a4a7935bd1e995LL;
  const int r = 47;

  uint64_t h = seed ^ (len * m);

  const uint64_t *data = (const uint64_t *)key;
  const uint64_t *end = data + (len / 8);

  while (data != end) {
    uint64_t k = *data++;

    k *= m;
    k ^= k >> r;
    k *= m;

    h ^= k;
    h *= m;
  }

  const unsigned char *data2 = (const unsigned char *)data;

  switch (len & 7) {
    case 7: h ^= uint64_t(data2[6]) << 48;
    case 6: h ^= uint64_t(data2[5]) << 40;
    case 5: h ^= uint64_t(data2[4]) << 32;
    case 4: h ^= uint64_t(data2[3]) << 24;
    case 3: h ^= uint64_t(data2[2]) << 16;
    case 2: h ^= uint64_t(data2[1]) << 8;
    case 1: h ^= uint64_t(data2[0]); h *= m;
  };

  h ^= h >> r;
  h *= m;
  h ^= h >> r;

  return h;
}






size_t hashmix(size_t x, size_t v)
{
  // From https://www.boost.org/doc/libs/1_85_0/libs/container_hash/doc/html/hash.html#notes_quality_of_the_hash_function
  if constexpr (sizeof(x) == 8 && sizeof(unsigned long long) == 8) {
    x ^= x >> 32;
    x *= 0xe9846af9b1a615dULL;
    x ^= x >> 32;
    x *= 0xe9846af9b1a615dULL;
    x ^= x >> 28;
  }
  else if constexpr (sizeof(x) == 4 && sizeof(unsigned int) == 4) {
    x ^= x >> 16;
    x *= 0x21f0aaadU;
    x ^= x >> 15;
    x *= 0x735a2d97U;
    x ^= x >> 15;      
  }
  else {
    // supposedly static_assert(false, "blah") will work in C++23.
    throw logic_error("Unknown sizeof(size_t)");
  }
  return x ^ v;
}

