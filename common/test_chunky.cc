
#include "./std_headers.h"
#include "./chunky.h"

#include "catch2/catch_all.hpp"


struct Foo : AllocTrackingMixin<Foo> {
  Foo() = default;
  Foo(Foo const &other) = default;
  Foo(Foo &&other) = default;
  Foo & operator = (Foo const &other) = default;
  Foo & operator = (Foo &&other) = default;
  string bar;
};

TEST_CASE("tracker", "[mem]")
{
  {
    Foo f;
    CHECK(AllocTrackingMixin<Foo>::tracker.nAlloc == 1);
  }
  CHECK(AllocTrackingMixin<Foo>::tracker.nAlloc == 0);

  {
    Foo f;
    auto g = f;
    CHECK(AllocTrackingMixin<Foo>::tracker.nAlloc == 2);
  }
  CHECK(AllocTrackingMixin<Foo>::tracker.nAlloc == 0);

  AllocTrackingMixin<Foo>::tracker.clearCounters();
  {
    Foo f;
    auto g = std::move(f);
    CHECK(AllocTrackingMixin<Foo>::tracker.nAlloc == 2);
  }
  CHECK(AllocTrackingMixin<Foo>::tracker.nAlloc == 0);
  CHECK(AllocTrackingMixin<Foo>::tracker.nMoveConstructed == 1);


  AllocTrackingMixin<Foo>::tracker.clearCounters();
  {
    Foo f;
    Foo g;
    g = std::move(f);
    CHECK(AllocTrackingMixin<Foo>::tracker.nAlloc == 2);
  }
  CHECK(AllocTrackingMixin<Foo>::tracker.nAlloc == 0);
  CHECK(AllocTrackingMixin<Foo>::tracker.nMoveAssigned == 1);


  AllocTrackingMixin<Foo>::tracker.clearCounters();
  {
    Foo f;
    Foo g;
    g = f;
    CHECK(AllocTrackingMixin<Foo>::tracker.nAlloc == 2);
  }
  CHECK(AllocTrackingMixin<Foo>::tracker.nAlloc == 0);
  CHECK(AllocTrackingMixin<Foo>::tracker.nCopyAssigned == 1);


  AllocTrackingMixin<Foo>::tracker.clearCounters();
  {
    vector<Foo> foos(23);
    CHECK(AllocTrackingMixin<Foo>::tracker.nAlloc == 23);
    foos.resize(35);
    CHECK(AllocTrackingMixin<Foo>::tracker.nAlloc == 35);
  }
  CHECK(AllocTrackingMixin<Foo>::tracker.nAlloc == 0);
  // This tests whether std::vector uses the move constructor when available
  CHECK(AllocTrackingMixin<Foo>::tracker.nMoveConstructed == 23);

}
